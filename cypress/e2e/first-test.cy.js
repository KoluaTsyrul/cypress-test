describe('Наш перший блок тестів', () => {
  it('Тест для відвідквання сторінки як покупець', () => {
    cy.visit("https://www.edu.goit.global/account/login");

    cy.signIn("user888@gmail.com", "1234567890");
  })

  it('Тест для відвідквання сторінки як менеджер', () => {
    cy.visit("https://www.edu.goit.global/account/login");

    cy.signIn("nadia.tsomko.98@gmail.com", "Nadia_Tsomko78");
  })

  it('Тест для відвідквання сторінки як адміністратор', () => {
    cy.visit("https://www.edu.goit.global/account/login");

    cy.signIn("mrdusty@duniakeliling.com", "mrdusty@duniakeliling.com");
  })

  it('Первірка властивостей кнопки', () => {
    cy.visit("https://www.edu.goit.global/account/login")

    cy.get('[type="submit"]').should("have.text", "Log in");

    cy.get('[type="submit"]').should("have.css", "background-color", "rgb(255, 107, 10)");
  })
})