describe('httpbin tests', () => {
  it('response code should be 200', () => {
    const request = {
      method: 'GET',
      url: 'https://reqres.in/api/users/2',
    };

    cy.request(request).then(resp => {
      debugger;

      const body = resp.body;

      const email = body.data.email;

      assert.equal("wrong_email@1", email);
    })
  })

  // it('stub test', () => {
  //   cy.intercept({
  //     method: 'GET',
  //     url: 'https://reqres.in/api/users/2',
  //   }).as('getUsers');
  // })
})