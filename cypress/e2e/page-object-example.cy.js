import { LoginPage } from "../pages/login";

const loginPage = new LoginPage();

describe("Page obj example", () => {
    beforeEach(() => {
        cy.visit("https://www.edu.goit.global/account/login");
    })

    it.skip('Login page', () => {
        // // Check our title
        // cy.get(".css-10stgr7 > .css-c1vj7d").should("be.visible");
        // cy.get(".css-10stgr7 > .css-c1vj7d").should("have.text", "Login");

        // // Check our inputs
        // cy.get("#user_email").should("be.visible");
        // cy.get("#user_password").should("be.visible");

        // // Check our button
        // cy.contains("Log in").should("be.visible");

        // // Check reset pass
        // cy.get("[href='/account/password/restore']").should("be.visible");
        // cy.get("[href='/account/password/restore']").should("have.text", "I can't remember the password");

        loginPage.navigateToPage();

        loginPage.validateLoginTitle();

        loginPage.validateInputs();

        loginPage.validateLogInButtun("Log in");

        loginPage.validatePasswordLink();
    })
})